.code
{
  font-family: Courier New;
  font-size: 10pt;
  background-color: #EEEEEE;
  margin: 0px;
  padding: 5px;
  border: dotted 1px #CCCCCC;
  overflow:auto;
}

/* Remove avatar and adjust spacing of other content in its place */
.abbreviated-post {
        padding-left:0
}

.abbreviated-post .post-author {
        display:none
}

.abbreviated-post .avatar, .content-fragment.blog-post .full-post .post-author .avatar {
        display:none
}

.content-fragment.blog-post .full-post .post-author {
        padding-left:0;
        margin-left:0
}
/* End avatar customization */
