<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8" dir="rtl" lang="he-IL">
<![endif]-->
<!--[if gt IE 8  ]><!-->
<html dir="rtl" lang="he-IL">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=EDGE">
<meta name="viewport" content="initial-scale=1.0, width=device-width" />
<title>העמוד לא נמצא | MS Israel Community</title>
<link rel="icon" type="image/x-icon" href="/favicon.ico" />
<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-114.png" />
<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114.png" />
<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-144.png" />
<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144.png" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="http://blogs.microsoft.co.il/xmlrpc.php" />
<link rel="stylesheet" type="text/css" href="http://blogs.microsoft.co.il/wp-content/themes/bloggers/FlexSlider/flexslider.css">
<link rel="stylesheet" type="text/css" href="http://blogs.microsoft.co.il/wp-content/themes/bloggers/colorbox.css">



<!--[if lt IE 9]>
<script src="http://blogs.microsoft.co.il/wp-content/themes/bloggers/js/html5.js" type="text/javascript"></script>
<![endif]-->




<link rel="alternate" type="application/rss+xml" title="MS Israel Community &raquo; פיד‏" href="http://blogs.microsoft.co.il/feed/" />
<link rel="alternate" type="application/rss+xml" title="MS Israel Community &raquo; פיד תגובות‏" href="http://blogs.microsoft.co.il/comments/feed/" />
<link rel='stylesheet' id='frm-forms-css'  href='http://blogs.microsoft.co.il/wp-content/plugins/formidable/css/frm_display.css?ver=1.07.01' type='text/css' media='all' />
<link rel='stylesheet' id='bloggers-style-css'  href='http://blogs.microsoft.co.il/wp-content/themes/bloggers/style.css?ver=4.1.1' type='text/css' media='all' />
<!--[if lt IE 9]>
<link rel='stylesheet' id='bloggers-ie-css'  href='http://blogs.microsoft.co.il/wp-content/themes/bloggers/css/ie.css?ver=20121010' type='text/css' media='all' />
<![endif]-->
<link rel='stylesheet' id='ms_global_search_css_style-css'  href='http://blogs.microsoft.co.il/wp-content/plugins/multisite-global-search/style.css?ver=4.1.1' type='text/css' media='all' />
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-includes/js/jquery/jquery.js?ver=1.11.1'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://blogs.microsoft.co.il/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://blogs.microsoft.co.il/wp-includes/wlwmanifest.xml" /> 
<link rel="stylesheet" href="http://blogs.microsoft.co.il/wp-content/themes/bloggers/rtl.css" type="text/css" media="screen" /><meta name="generator" content="WordPress 4.1.1" />

<!-- ADD TO HEADER - START -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
 
  ga('create', 'UA-34806005-2', 'microsoft.co.il');
  ga('send', 'pageview');
 
</script>
<!--Wise pixel Microsoft Blogs-->
<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=661206&mt_adid=123765&v1=&v2=&v3=&s1=&s2=&s3='></script>

<script type="text/javascript">
jQuery(document).ready(function()
{
jQuery('form:not(.ms-global-search_vbox searchb, #searchform)').attr('autocomplete','off');
});
</script>



<!-- ADD TO HEADER - END -->
<meta name="google-publisher-plugin-pagetype" content="errorPage"><style type="text/css" id="syntaxhighlighteranchor"></style>
  <script src="http://blogs.microsoft.co.il/wp-content/themes/bloggers/FlexSlider/jquery.flexslider-min.js"></script>	
  <script src="http://blogs.microsoft.co.il/wp-content/themes/bloggers/js/jquery.colorbox-min.js"></script>	
  <script src="http://blogs.microsoft.co.il/wp-content/themes/bloggers/js/app.js"></script>
        
     <!--Wise pixel Microsoft Blogs-->
<script language='JavaScript1.1' src='//pixel.mathtag.com/event/js?mt_id=661206&mt_adid=123765&v1=&v2=&v3=&s1=&s2=&s3='></script>

</head>

<body class="rtl error404 single-author  body" >
	
	<header id="masthead" class="site-header" role="banner">
		<div class="top-line ">
            <div class="wrapper clearfix">
		<a href="http://blogs.microsoft.co.il" class=" header_logo"></a>
		<ul class="resp_header">
            <li class="openblog-holder">
		<a href="https://blogs.microsoft.co.il/wp-signup.php" class="sprite open_blog"><span>פתח בלוג</span></a>
			</li>
	<li>
		                 
            <a href="https://blogs.microsoft.co.il/wp-login.php">התחבר</a>
              			</li>
           
		    <li id="menu-item-64" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-64"><a href="http://blogs.microsoft.co.il/contact/">צור קשר</a></li>
<li id="menu-item-146" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-146"><a href="http://blogs.microsoft.co.il/help/">עזרה</a></li>
              
			
            <li class="search-holder">
			
				<form class="ms-global-search_form" method="get" action="http://blogs.microsoft.co.il/globalsearch/">
			<div>
			  
			    <input class="ms-global-search_vbox searchb" name="mssearch" type="text" value="" size="16" tabindex="1" />
		
			    <input type="submit" class="sprite searchb" value="&nbsp;" tabindex="2" />
			
			
		    </div>
	    </form>
               </li>
			
		
			</ul> <!-- end resp -->
			</div>
				
			</div>	
   
		</header><!-- #masthead -->
	
	<div class="clear"></div>
	
		 
		
		    <div class="main_banner signupbanner" style="background-image: url(http://blogs.microsoft.co.il/wp-content/themes/bloggers/img/main_banner.jpg)">
		</div>
		
				
		<div id="page" class="hfeed site">  
		<div id="main" class="wrapper clearfix">
		
	<div id="primary" class="site-content">
		<div id="content" role="main">
  <h2 class="sub-title">העמוד שביקשת אינו קיים</h2>
				<div id="primary_blog">
<div class="p-content post no-results not-found">


				<form role="search" method="get" id="searchform" class="searchform" action="http://blogs.microsoft.co.il/">
				<div>
					<label class="screen-reader-text" for="s">חפש:</label>
					<input type="text" value="" name="s" id="s" />
					<input type="submit" id="searchsubmit" value="חפש" />
				</div>
			</form>
		</div>
			
		
	</div>

		</div><!-- #content -->
	</div><!-- #primary -->

	</div><!-- #page -->
	</div><!-- #main .wrapper -->
	<footer class="footer_main clearfix">
	<section class="wrapper clearfix">
		<article id="first_fl" class="footer_list">
		<h3>משאבים לקהילת המפתחים</h3>
			<div class="menu-footer1-container">
				<ul id="menu-footer-menu-2">
				<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://blogs.microsoft.co.il/msdn" title="MSDN בלוג" target="_blank">בלוג MSDN</a></li>
				<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.microsoft.com/israel/msdn/pulse/" title="ניוזלטר  MSDN" target="_blank">ניוזלטר  MSDN</a></li>
				<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.facebook.com/msdnisrael" title="MSDN בפייסבוק" target="_blank">בפייסבוק MSDN</a></li>
				<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.youtube.com/channel/UCp6wuFkya3jOETs_nM7z3Fw?feature=watch" target="_blank">יוטיוב  MSDN</a></li>
				<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.microsoftvirtualacademy.com/"  target="_blank">האקדמיה הוירטואלית</a></li>
				<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://msdn.microsoft.com/en-us/evalcenter/default"  target="_blank">התנסות חינם</a></li>
				<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.microsoft.com/israel/4steps/"  target="_blank">אתר פיתוח אפליקציות</a></li>
				</ul>
			</div>
		</article>
			<article id="sec_fl" class="footer_list">
		<h3>משאבים לקהילת IT</h3>
				<ul id="menu-footer-menu-2">
				<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://blogs.microsoft.co.il/technet" title="TechNet בלוג"  target="_blank">בלוג TechNet </a></li>
				<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.microsoft.com/israel/technetflash/Prev_vers/index.htm" target="_blank">ניוזלטר  TechNet </a></li>
				<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.facebook.com/technetil" title="TechNet בפייסבוק"  target="_blank">בפייסבוק TechNet</a></li>
				<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.youtube.com/user/TechNetIsrael/videos" target="_blank">יוטיוב  TechNet</a></li>
								<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.microsoftvirtualacademy.com/"  target="_blank">האקדמיה הוירטואלית</a></li>
				<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://technet.microsoft.com/en-us/evalcenter/default"  target="_blank">התנסות חינם</a></li>
				</ul>
			</article>
			<article id="thrd_fl" class="footer_list">
		<h3>משאבים כללים </h3>
					<ul id="menu-footer-menu-2">
						<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.microsoft.com/he-il/default.aspx" target="_blank" title="אתר מיקרוסופט ישראל">אתר מיקרוסופט ישראל</a></li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.facebook.com/microsoftisrael" target="_blank" title="מיקרוסופט ישראל בפייסבוק">מיקרוסופט ישראל בפייסבוק</a></li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://social.msdn.microsoft.com/Forums/he-il/categories" target="_blank" title=">אתר הפורומים">אתר הפורומים</a></li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.microsoft.com/israel/communities/usergroups/default.aspx" target="_blank" title=">קבוצות משתמשים">קבוצות משתמשים</a></li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://www.microsoft.com/israel/traincert/" target="_blank" title="הדרכות והסמכות מיקרוסופט">הדרכות והסמכות מיקרוסופט</a></li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://msevents.microsoft.com/CUI/default.aspx?culture=he-IL" target="_blank">אירועים </a></li>
					
					<!--
					<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://blogs.microsoft.co.il/סימנים-מסחריים/" title=" סימנים מסחריים"> סימנים מסחריים</a></li>
				<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://blogs.microsoft.co.il/תנאי-שימוש/" title="תנאי שימוש">תנאי שימוש</a></li>
-->
				</ul>	</article>
		</section>
		<section class="sfooter">
                <div class="content">
                    <div class="footer-bottom">                        
                        <ul class="legal">
                            <li><a href="http://choice.microsoft.com/">אודות הפרסומות שלנו</a></li>
                            <li><a href="http://www.microsoft.com/About/Legal/EN/US/IntellectualProperty/Trademarks/EN-US.aspx">סימנים מסחריים</a></li>
                            <li><a href="http://www.microsoft.com/en-us/legal/intellectualproperty/copyright/default.aspx">תנאי השימוש</a></li>
                            <li ><a href="http://go.microsoft.com/fwlink/?LinkId=248681">הצהרת פרטיות</a></li>
                        </ul>
                        <div class="footer-copyright">
                            <a href="http://www.microsoft.com">
                                <img alt="Microsoft" src="http://www.microsoft.com/israel/azure/img/microsoft-logo-heb.png">
                            </a><span>© 2014 Microsoft</span>
                        </div>
                    </div>
                </div>
</section>
	</footer><!-- #colophon -->

<!-- Do Not Remove - Turn Tracking Beacon Code - Do Not Remove -->
<!-- Advertiser Name : microsoft -->
<!-- Beacon Name : Microsoft_ blogs general -->
<!-- If Beacon is placed on a Transaction or Lead Generation based page, please populate the turn_client_track_id with your order/confirmation ID -->
<script type="text/javascript">
  turn_client_track_id = "";
</script>
<script type="text/javascript" src="http://r.turn.com/server/beacon_call.js?b2=PGyOFcFmMNXjqLZWhsqf0IYBbGja1CD2b3Qxz0EXcZ4jZElP3T1utc_BawhZaoxwRafeYDFgKVEXkxLRf3dk_A">
</script>
<noscript>
  <img border="0" src="http://r.turn.com/r/beacon?b2=PGyOFcFmMNXjqLZWhsqf0IYBbGja1CD2b3Qxz0EXcZ4jZElP3T1utc_BawhZaoxwRafeYDFgKVEXkxLRf3dk_A&cid=">
</noscript>
<!-- End Turn Tracking Beacon Code Do Not Remove -->


<script>
(function() { // Redistats, track version 1.0
	var global_id = 147;
	var property_id = 1;
	var url = encodeURIComponent(window.location.href.split('#')[0]);
	var referrer = encodeURIComponent(document.referrer);
	var x = document.createElement('script'), s = document.getElementsByTagName('script')[0];
	x.src = '//redistats.com/track.js?gid='+global_id+'&pid='+property_id+'&url='+url+'&referrer='+referrer;
	s.parentNode.insertBefore(x, s);
})();
</script><script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shCore.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushAS3.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushBash.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushColdFusion.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/third-party-brushes/shBrushClojure.js?ver=20090602'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushCpp.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushCSharp.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushCss.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushDelphi.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushDiff.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushErlang.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/third-party-brushes/shBrushFSharp.js?ver=20091003'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushGroovy.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushJava.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushJavaFX.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushJScript.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/third-party-brushes/shBrushLatex.js?ver=20090613'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/third-party-brushes/shBrushMatlabKey.js?ver=20091209'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/third-party-brushes/shBrushObjC.js?ver=20091207'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushPerl.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushPhp.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushPlain.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushPowerShell.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushPython.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/third-party-brushes/shBrushR.js?ver=20100919'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushRuby.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushScala.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushSql.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushVb.js?ver=3.0.9b'></script>
<script type='text/javascript' src='http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/scripts/shBrushXml.js?ver=3.0.9b'></script>
<script type='text/javascript'>
	(function(){
		var corecss = document.createElement('link');
		var themecss = document.createElement('link');
		var corecssurl = "http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/styles/shCore.css?ver=3.0.9b";
		if ( corecss.setAttribute ) {
				corecss.setAttribute( "rel", "stylesheet" );
				corecss.setAttribute( "type", "text/css" );
				corecss.setAttribute( "href", corecssurl );
		} else {
				corecss.rel = "stylesheet";
				corecss.href = corecssurl;
		}
		document.getElementsByTagName("head")[0].insertBefore( corecss, document.getElementById("syntaxhighlighteranchor") );
		var themecssurl = "http://blogs.microsoft.co.il/wp-content/plugins/syntaxhighlighter/syntaxhighlighter3/styles/shThemeDefault.css?ver=3.0.9b";
		if ( themecss.setAttribute ) {
				themecss.setAttribute( "rel", "stylesheet" );
				themecss.setAttribute( "type", "text/css" );
				themecss.setAttribute( "href", themecssurl );
		} else {
				themecss.rel = "stylesheet";
				themecss.href = themecssurl;
		}
		//document.getElementById("syntaxhighlighteranchor").appendChild(themecss);
		document.getElementsByTagName("head")[0].insertBefore( themecss, document.getElementById("syntaxhighlighteranchor") );
	})();
	SyntaxHighlighter.config.strings.expandSource = '+ expand source';
	SyntaxHighlighter.config.strings.help = '?';
	SyntaxHighlighter.config.strings.alert = 'SyntaxHighlighter\n\n';
	SyntaxHighlighter.config.strings.noBrush = 'Can\'t find brush for: ';
	SyntaxHighlighter.config.strings.brushNotHtmlScript = 'Brush wasn\'t configured for html-script option: ';
	SyntaxHighlighter.defaults['pad-line-numbers'] = false;
	SyntaxHighlighter.defaults['toolbar'] = false;
	SyntaxHighlighter.all();
</script>
</body>
</html>