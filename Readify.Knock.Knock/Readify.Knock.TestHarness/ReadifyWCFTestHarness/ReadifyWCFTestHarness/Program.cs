﻿using System.Diagnostics;

namespace ReadifyWCFTestHarness
{
    class Program
    {
        static void Main(string[] args)
        {
            var stopWatch = new Stopwatch();
            var client = new ReadifyServiceReference.RedPillClient();
            
            stopWatch.Start();
            var fibonacci = client.FibonacciNumber(6);
            stopWatch.Stop();
            Debug.WriteLine(stopWatch.ElapsedMilliseconds);
            Debug.WriteLine(fibonacci);

            stopWatch.Reset();
            stopWatch.Start();                
            var shape = client.WhatShapeIsThis(1, 2, 3);
            stopWatch.Stop();
            Debug.WriteLine(stopWatch.ElapsedMilliseconds);
            Debug.WriteLine(shape);

            stopWatch.Reset();
            stopWatch.Start();
            var reverse = client.ReverseWords("123AbC");
            stopWatch.Stop();
            Debug.WriteLine(stopWatch.ElapsedMilliseconds);
            Debug.WriteLine(reverse);
        }
    }
}
