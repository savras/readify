﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCFServiceWebRole;

namespace WCFUnitTests
{
    [TestClass]
    public class RedPillUnitTests
    {
        RedPill redPill;

        [TestInitialize]
        public void Setup()
        {
            redPill  = new RedPill();
        }

        [TestMethod]
        public void GivenString_ReverseString_CorrectlyReversesString()
        {
            // Arrange
            var input = "ReadifY0123";

            // Act
            var result = redPill.ReverseWords(input);

            // Assert
            Assert.IsTrue(result == "3210YfidaeR");
        }

        // Bloody VS Express doesn't come with test runner..
        [TestMethod, Ignore]
        public void TheOtherTests()
        {

        }
    }
}
