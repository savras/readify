﻿using knockknock.readify.net;
using System;
using System.Collections.Generic;
using System.Text;

namespace WCFServiceWebRole
{
    public class RedPill : IRedPill
    {
        public static Dictionary<long, long> FibonacciDictionary = new Dictionary<long, long>();

        public TriangleType WhatShapeIsThis(int a, int b, int c)
        {
            var triangleType = TriangleType.Error;

            if (a <= 0 || b <= 0 || c <= 0)
            {
                // Just return error.
            }
            else if (!(a + b > c) || !(a + c > b) || !(b + c > a))
            {
                // Not a valid triangle
            }
            else if (a == b && b == c && c == a)
            {
                triangleType = TriangleType.Equilateral;
            }
            else if (a == b || a == c || b == c)
            {
                triangleType = TriangleType.Isosceles;
            }
            else if ((a != b && b != c && c != a))
            {
                triangleType = TriangleType.Scalene;
            }

            return triangleType;
        }

        public long FibonacciNumber(long n)
        {
            long originalN = n;
            n = Math.Abs(n);

            if(n > 92)
            {
                throw new ArgumentOutOfRangeException("n", "Fib(>92) will cause a 64-bit integer overflow.");
            }

            if (n <= 0) { return 0; }
            if (n == 1) { return 1; }

            long prev = 0;
            if (!FibonacciDictionary.TryGetValue(n - 2, out prev))
            {
                prev = FibonacciNumber(n - 2);
                FibonacciDictionary.Add(n - 2, prev);
            }

            long current = 0;
            if (!FibonacciDictionary.TryGetValue(n - 1, out current))
            {
                current = FibonacciNumber(n - 1);
                FibonacciDictionary.Add(n - 1, current);
            }

            var result = current + prev;
            if (originalN < 0 && originalN % 2 == 0)
            {
                result *= -1;
            }
            
            return result;
        }

        public string ReverseWords(string input)
        {
            if(string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }

            var split = input.Split();
            
            var offset = 1;
            var stringBuilder = new StringBuilder();
            for (int p = 0; p < split.Length; p++)
            {
                for (var i = split[p].Length - offset; i >= 0; i--)
                {
                    stringBuilder.Append(split[p][i]);
                }
                stringBuilder.Append(" ");
            }
            return stringBuilder.ToString().Substring(0, stringBuilder.Length - 1) ;
        }

        public Guid WhatIsYourToken()
        {
            return Guid.NewGuid();
        }
    }
}
